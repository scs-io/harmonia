/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
 * <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
 * Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Harmonia
 * 
 * Harmonia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by anthony on 6/19/17.
//

#ifndef HARMONIA_INTERFERENCE_TESTS_H
#define HARMONIA_INTERFERENCE_TESTS_H

#include <vector>
#include <string>
#include <mpi.h>
#include <sstream>
#include <cstdint>
#include <cstring>
#include <unistd.h>
#include <sys/syscall.h>
#include <pwd.h>

class interference_tests {
private:
  struct pattern {
    size_t offset[6150];
    size_t request[6150];
    long total_size;
    int number_of_lines;
  };
  int argc;
  char **argv;
  char name[256];
  char *buffer;
  int option;
  MPI_Comm group_comm;
  std::string trace_file;
  std::string trace;
  pattern access;

public:
  int rank,comm_size,group_comm_size;
  char destination[256];
  double endTime;
  int repetitions,groupkey,num_of_applications,group_rank;
  std::vector<int> delay_between_apps;
  int access_mode;
  int operation_mode;
  size_t request_size;
  long burst_size;
  int direct;
  int application_pattern,delay_between_bursts;
  char log_dir[256];
  size_t total_io_per_application = 1024L*1024L*1024L;
  interference_tests(int argc, char *argv[]){
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);  /* get current process id */
    MPI_Comm_size(MPI_COMM_WORLD, &comm_size);
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
    this->num_of_applications=1;
    group_comm_size=comm_size/num_of_applications;
    this->argc=argc;
    this->argv=argv;
    this->repetitions=1;
    this->direct=0;
    this->access_mode=0;
    this->request_size=total_io_per_application/group_comm_size;
    strcpy(destination,homedir);
    strcpy(log_dir,homedir);
    this->operation_mode=0;
    this->application_pattern=0;
    this->burst_size=1*1024*1024;
    this->delay_between_bursts=0;
    this->delay_between_apps=std::vector<int>();
    this->delay_between_apps.push_back(0);
  }
  void mpi_init();
  void mpi_finalize();
  inline std::vector<std::string> split_string(std::string input,
                                               char delimiter){
    std::vector<std::string> tokens = std::vector<std::string>();
    std::istringstream ss(input);
    std::string token;
    while(std::getline(ss, token, delimiter)) {
      tokens.emplace_back(token);
    }
    return tokens;
  }

  void prepare_buffers();
  void delete_data();
  void get_input_parameters();
  double run_io();
  double run_system_call_io();


};

#endif //HARMONIA_INTERFERENCE_TESTS_H
