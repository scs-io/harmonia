/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
 * <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
 * Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Harmonia
 * 
 * Harmonia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */
//
// Created by anthony on 6/19/17.
//

#include <iterator>
#include <zconf.h>
#include "interference_tests.h"
#include "../tools/Timer.h"
#include <string.h>
#include <iostream>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stropts.h>
#include <sys/stat.h>

static void handle_error(int errcode, char *str)
{       
    char msg[MPI_MAX_ERROR_STRING];
    int resultlen;
    MPI_Error_string(errcode, msg, &resultlen);
    fprintf(stderr, "%s: %s\n", str, msg);
    MPI_Abort(MPI_COMM_WORLD, 1);
}
#define MPI_CHECK(fn) { int errcode; errcode = (fn);\
     if (errcode != MPI_SUCCESS) handle_error  (errcode, #fn ); }


void interference_tests::mpi_init() {
  //Initialize MPI
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);  /* get current process id */
  MPI_Comm_size(MPI_COMM_WORLD, &comm_size); /* get number of processes */
  if (comm_size % num_of_applications !=0) {
    printf("Your split into %d applications is wrong\n",num_of_applications);
    printf("Please run_trace the program again!\n");
    MPI_Finalize();
    exit(0);
  }
  MPI_Barrier(MPI_COMM_WORLD);
  groupkey = rank % num_of_applications;
  MPI_Comm_split(MPI_COMM_WORLD, groupkey, rank, &group_comm);
  group_comm_size=comm_size/num_of_applications;
  MPI_Comm_rank(group_comm, &group_rank);
}
void interference_tests::mpi_finalize() {
  //Free communicators and Finalize
  MPI_Comm_free(&group_comm);
  MPI_Finalize();
}
void interference_tests::delete_data() {
  int stat;
  MPI_Barrier(MPI_COMM_WORLD);
  //Remove temp files with data
  if(operation_mode==0 && application_pattern==0){
    if(access_mode==0){
      sprintf(name, "%s/file_rank_%d_group_%d.txt",destination, group_rank, groupkey);
    }else{
      sprintf(name, "%s/file_group_%d.txt",destination, groupkey);
    }
    stat = remove(name);
    if (stat == 1) printf("Error deleting temp file %s!\n", name);
  }
  if(buffer) free(buffer);
  MPI_Barrier(MPI_COMM_WORLD);
}
void interference_tests::get_input_parameters() {
//check for the number of command line arguments
  /*if (argc != 23) {
    printf("There are less arguments passed\n");
    printf("Usage: %s\n", argv[0]);
    printf("-m : #Apps\n");
    printf("-d : Delay(ms) between apps\n");
    printf("-r : #Repetitions\n");
    printf("-a : Access pattern(0->file-per-rank, 1->file-per-group)\n");
    printf("-s : Request size in KB\n");
    printf("-i : Destination medium\n");
    printf("-o : Operation mode(0-> Read, 1-> Write)\n");
    printf("-w : Application pattern(0-> Sequential, 1-> Random)\n");
    printf("-b : Burst KB\n");
    printf("-x : Delay(ms) within Bursts\n");
    printf("-l : Log Dir\n");
    exit(0);
  }*/

//Loop through the arguments given by user
  while ((option = getopt(argc, argv, "m:d:r:a:s:i:o:w:b:x:l:t:c:h:")) != -1) {
    switch (option) {
      case 'm':
        num_of_applications = atoi(optarg);
        break;

      case 'r':
        repetitions = atoi(optarg);
        break;

      case 'a':
        access_mode = atoi(optarg);
        break;

      case 's':
        request_size = atol(optarg)*1024L;
        break;

      case 'i':
        strcpy(destination,optarg);
        break;
      case 'l':
        strcpy(log_dir,optarg);
        break;
      case 'o':
        operation_mode = atoi(optarg);
        break;
      case 'w':
        application_pattern = atoi(optarg);
        break;
      case 'b':
        burst_size = atoi(optarg)*1024;
        break;
      case 'x':
        delay_between_bursts = atoi(optarg);
        break;
      case 't':
        total_io_per_application = atol(optarg)*1024L*1024L;
        break;
      case 'c':
        direct = atol(optarg);
        break;
      case 'h':
        /*'?' */
        printf("There are less arguments passed\n");
        printf("Usage: %s\n", argv[0]);
        printf("-m : #Apps\n");
        printf("-d : Delay(ms) between apps\n");
        printf("-r : #Repetitions\n");
        printf("-a : Access pattern(0->file-per-rank, 1->file-per-group)\n");
        printf("-s : Request size in KB\n");
        printf("-i : Destination medium\n");
        printf("-o : Operation mode(1-> Read, 0-> Write)\n");
        printf("-w : Application pattern(0-> Sequential, 1-> Random)\n");
        printf("-b : Burst KB\n");
        printf("-x : Delay(ms) within Bursts\n");
        printf("-t : Total size in MB\n");
        printf("-c : Direct IO 1/0\n");
      case 'd':
        delay_between_apps=std::vector<int>();
        std::vector<std::string> temp = split_string(optarg,',');
        for(auto val:temp){
          delay_between_apps.push_back(atoi(val.c_str()));
        }
        break;
    }
  }
}
void interference_tests::prepare_buffers() {
  MPI_Comm_rank(group_comm, &group_rank);
  buffer = static_cast<char *>(calloc(total_io_per_application/group_comm_size,sizeof(char)));
  MPI_Barrier(MPI_COMM_WORLD);

}
double interference_tests::run_io() {
  int count=0;
  Timer timer = Timer();
  endTime=0;
  auto num_iterations = total_io_per_application/(request_size*group_comm_size);
  if(num_iterations==0){
    num_iterations=num_iterations==0?1:num_iterations;
    request_size=request_size/group_comm_size;
  }
  //start looping to do the I/O
  MPI_Comm_rank(group_comm, &group_rank);
  MPI_File fh;
  int access_multiplier=1;

  if(access_mode==0){
    access_multiplier=0;
    if(operation_mode==1){
      sprintf(name, "%s/file_rank_%d_group_%d.txt",destination, group_rank, groupkey);
    }else{
      sprintf(name, "%s/file_rank_%d_group_%d.txt",destination, group_rank, groupkey);
    }
    MPI_CHECK(MPI_File_open(MPI_COMM_SELF, name,MPI_MODE_CREATE | MPI_MODE_RDWR, MPI_INFO_NULL,
                  &fh));
  }else{
    access_multiplier=group_rank;
    if(operation_mode==1){
      sprintf(name, "%s/file_group_%d.txt",destination, groupkey);
    }else{
      sprintf(name, "%s/file_group_%d.txt",destination, groupkey);
    }
    MPI_File_open(group_comm, name,MPI_MODE_CREATE | MPI_MODE_RDWR, MPI_INFO_NULL,
                  &fh);
  }
  if(fh== nullptr) std::cout << "Error opening the file" << name << "\n";
  MPI_Offset my_offset;
  MPI_Status status;
  long currentBurst=0;
  if(application_pattern==0){
      my_offset = (long long) access_multiplier* (long long) total_io_per_application/group_comm_size;
      MPI_File_seek(fh, my_offset, MPI_SEEK_SET);
  }
  for (int i = 0; i < num_iterations; ++i) {
    timer.startTime();
    if(application_pattern==1){
      if(access_mode==1)
        my_offset = rand()%(((total_io_per_application - request_size)-request_size) +1)+ request_size;
      else
        my_offset = rand()%(((total_io_per_application/group_comm_size - request_size)-request_size) +1)+ request_size;
      MPI_File_seek(fh, my_offset, MPI_SEEK_SET);
    }
    if(operation_mode==0){
      MPI_File_write(fh, &buffer[i * request_size],
                     static_cast<int>(request_size), MPI_CHAR,
                     &status);
      MPI_Get_count(&status, MPI_CHAR, &count);
      if(count!=request_size) std::cout << "Error writing count: "<< count
                                        << " request size:" <<request_size
                                        << "\n";
    }else{
      MPI_File_read(fh, &buffer[i * request_size],
                    static_cast<int>(request_size), MPI_CHAR, &status);
      MPI_Get_count(&status, MPI_CHAR, &count);
      if(count!=request_size) std::cout << "Error reading count: "<< count
                                        << " request size:" <<request_size
                                        << "\n";
    }
    currentBurst+=request_size;
    if(currentBurst>=burst_size){
      currentBurst=0;
      usleep(static_cast<__useconds_t>(delay_between_bursts * 1000));
    }
    endTime += timer.endTimeWithoutPrint("");
  }
  MPI_File_close(&fh);

  double group_time;
  MPI_Allreduce(&endTime, &group_time, 1, MPI_DOUBLE, MPI_SUM, group_comm);
  return group_time/(comm_size/num_of_applications);
}

double interference_tests::run_system_call_io() {
  int fd;
  ssize_t count=0;
  Timer timer = Timer();
  endTime=0;
  auto num_iterations = total_io_per_application/(request_size*group_comm_size);
  if(num_iterations==0){
    num_iterations=1;
    request_size=total_io_per_application/group_comm_size;
  }
  //start looping to do the I/O
  sprintf(name, "%s/file_rank_%d_group_%d.txt",destination, group_rank, groupkey);
  /*if(destination=="/dev/shm/harmonia") fd = open(name,O_RDWR|O_CREAT|O_DSYNC,
                                               0666);*/
  if(direct) fd = open(name,O_RDWR|O_CREAT|O_DSYNC|O_DIRECT,0666);
  else fd = open(name,O_RDWR|O_CREAT|O_DSYNC,0666);

  if(fd==-1) {
    printf("errno is %d and fd is %d name %s\n",errno,fd,name);
    std::cout << "Error opening" << " name:" <<name
              << "\n";
  }
  long long my_offset;
  long currentBurst=0;
  if(application_pattern==0){
    my_offset = 0;
    lseek(fd, my_offset, SEEK_SET);
  }
  for (int i = 0; i < num_iterations; ++i) {
    timer.startTime();
    if(application_pattern==1){
        my_offset = static_cast<long long int>(rand() % (((total_io_per_application / group_comm_size - request_size) - request_size) + 1) + request_size);
      lseek(fd, my_offset, SEEK_SET);
    }
    if(operation_mode==0){
      count=write(fd, buffer, request_size);

    }else{
      count=read(fd, buffer, request_size);
    }
    ioctl(fd, I_FLUSH ,FLUSHRW);
    if(count != request_size){
      std::cout << "Error writing count: "<<count << " request size:" <<request_size
                << "\n";
    }
    currentBurst+=request_size;
    if(currentBurst>=burst_size){
      currentBurst=0;
      usleep(static_cast<__useconds_t>(delay_between_bursts * 1000));
    }
    endTime += timer.endTimeWithoutPrint("");
  }
  close(fd);
  double group_time;
  MPI_Allreduce(&endTime, &group_time, 1, MPI_DOUBLE, MPI_SUM, group_comm);
  return group_time/(comm_size/num_of_applications);
}

