/*
 * Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
 * <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
 * Xian-He Sun <sun@iit.edu>
 *
 * This file is part of Harmonia
 * 
 * Harmonia is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/>.
 */

#include "testing/interference_tests.h"
#include "tools/Timer.h"
#include <iostream>
#include <string.h>

int main(int argc, char *argv[]) {
//  freopen( "run.log", "a", stdout ); 
  interference_tests instance=interference_tests(argc, argv);
  instance.get_input_parameters();
  instance.mpi_init();
  std::string log(instance.log_dir);
  //freopen((log+"/run"+std::to_string(instance.rank)).c_str(), "w", stdout );
  double average_time=0;
  for(int i=0; i< instance.repetitions; ++i){
    instance.delete_data();
    instance.prepare_buffers();
    system("sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'");
    average_time+=instance.run_system_call_io();
    //instance.delete_data();
    system("sudo sh -c 'echo 3 > /proc/sys/vm/drop_caches'");
  }
  average_time=average_time / instance.repetitions;
  if(instance.rank ==0) {
    std::cout << "NumApps," << instance.num_of_applications
              << ",Ranks/App," << instance.group_comm_size
        //<< ",AccessMode," << instance.access_mode
              << ",IOperApp," << instance.request_size
              << ",Seq/Rand," << instance.application_pattern
              << ",Write/Read," << instance.operation_mode
              << ",Medium," << instance.destination
              << ",App," << instance.groupkey
              << ",Time," << average_time
              << std::endl;
  }
  
  instance.mpi_finalize();
  if(instance.rank==0){
    //system(("cat "+log+"/run* >> "+log+"/test.log").c_str());
    //system(("rm -f "+log+"/run*").c_str());
  }
  return 0;
}

