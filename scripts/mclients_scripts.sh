#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
declare -a devices=([0]="ram" [1]="nvme" [2]="ssd" [3]="hdd" )
declare -a ports=( [0]="3334" [1]="3335" [2]="3336" [3]="3337")
for app in 1 #2 3 4
do
for ranks in $(($app*1)) #$(($app*4)) $(($app*16)) $(($app*32))
do
for index in "${!devices[@]}";
do
export device=${devices[${index}]}
export port=${ports[$index]}
echo "umounting devices on clients ${device} ${port}"
for c in $(seq 1 4)
do
ssh harmonia-client-${c} "sudo umount --force /media/ram"
ssh harmonia-client-${c} "sudo umount --force /media/nvme"
ssh harmonia-client-${c} "sudo umount --force /media/ssd"
ssh harmonia-client-${c} "sudo umount --force /media/hdd"
done
echo "Starting server on device ${device}"
for s in $(seq 1 1)
do
ssh harmonia-bb-${s} "sudo killall pvfs2-server"
ssh harmonia-bb-${s} "sudo chown -R cc:cc /media"
ssh harmonia-bb-${s} "sudo mkdir -p /media/${device}/storage"
ssh harmonia-bb-${s} "sudo rm -rf /media/${device}/storage/*"

ssh harmonia-bb-${s} "/home/cc/software/install/sbin/pvfs2-server -f -a  harmonia-bb-${s} /home/cc/nfs/conf/${device}.conf"
ssh harmonia-bb-${s} "/home/cc/software/install/sbin/pvfs2-server -a  harmonia-bb-${s} /home/cc/nfs/conf/${device}.conf"
ssh harmonia-bb-${s} "ps -aef | grep pvfs2"
done
echo "mounting device on clients"
for c in $(seq 1 4)
do
ssh harmonia-client-${c} "sudo insmod /lib/modules/4.4.0-93-generic/kernel/fs/pvfs2/pvfs2.ko"
ssh harmonia-client-${c} "sudo killall pvfs2-client"
ssh harmonia-client-${c} "sudo /home/cc/software/install/sbin/pvfs2-client -p /home/cc/software/install/sbin/pvfs2-client-core"
ssh harmonia-client-${c} "sudo mount -t pvfs2 tcp://harmonia-bb-1:${port}/orangefs /media/${device}"
ssh harmonia-client-${c} "sudo chown -R cc:cc /media"
ssh harmonia-client-${c} "rm /media/${device}/*"
ssh harmonia-client-${c} "ls -l /media/${device}"
done
echo "running test on ${device}"
cd /home/cc/nfs/harmonia/scripts/
#mpirun -np 1 -f /home/cc/nfs/harmonia/nodes4 /home/cc/nfs/harmonia/harmonia -m 1 -d 0 -r 1 -a 0 -s 1024 -i /media/${device} -o 0 -w 0 -b 1024 -x 0 -l /home/cc/nfs/harmonia/logs
./scripts3.sh /media/${device} $app $ranks
#mv ../logs/test.log ../logs/intereference_${device}_scale.csv
for c in $(seq 1 4)
do
ssh harmonia-client-${c} "sudo umount --force /media/${device}"
done
cd /home/cc/nfs/harmonia/
#git add --all
#git commit -m "added log for ${device}"
#git push https://hdevarajan:samsung2404@bitbucket.org/akougkas/harmonia.git --all
done
done
done
