#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
if [ -d /etc/profile.d ]; then
  for i in /etc/profile.d/*.sh; do
    if [ -r $i ]; then
      . $i
    fi
  done
  unset i
fi
NODES=$(cat /home/cc/nfs/harmonia/scripts/nodes)
for mp in 1 2 3 4
do
	echo "refreshing servers and client start"
	/home/cc/nfs/harmonia/scripts/stop_servers.sh
	/home/cc/nfs/harmonia/scripts/servers_ram_start.sh
	/home/cc/nfs/harmonia/scripts/start_clients.sh
	echo "refreshing servers and client stop"
	for device in 1 2 3 4
	do
		for np in $((mp*128)) 
		do
			for op in 0 1
                	do
				for node in $NODES
				do
					ssh -t ${node} "/home/cc/nfs/harmonia/scripts/dropcache.sh"
				done
				echo "mpirun -n $np -f /home/cc/nfs/harmonia/nodes /home/cc/nfs/harmonia/harmonia -m ${mp} -t 32768 -s 32 -i /mnt/${device}pfs -r 5 -o $op -c 1 -w 0 >> /home/cc/nfs/harmonia/logs/main_results_ram.log"
				mpirun -n $np -f /home/cc/nfs/harmonia/nodes /home/cc/nfs/harmonia/harmonia -m ${mp} -t 16384 -s 32 -i /mnt/${device}pfs -r 5 -o $op -c 1 -w 0 >> /home/cc/nfs/harmonia/logs/main_results_ram.log
				sleep 3
				for node in $NODES
                        	do
                                	ssh -t ${node} "/home/cc/nfs/harmonia/scripts/dropcache.sh"
                        	done
			done
		done
	done
done
