# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
clear
NODES=$(cat /home/cc/nfs/harmonia/scripts/nodes)
i=20
for server_node in $NODES
do    
echo "starting server $server_node"
ssh -t $server_node << EOF
cat > /home/cc/interfaces <<EOL
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

# The loopback network interface
auto lo
iface lo inet loopback
# Source interfaces
# Please check /etc/network/interfaces.d before changing this file
# as interfaces may have been defined in /etc/network/interfaces.d
# See LP: #1262951
source /etc/network/interfaces.d/*.cfg
EOL
cat > /home/cc/ib0 <<EOL1
auto ib0
iface ib0 inet static
        address 192.168.4.${i}
        netmask 255.255.255.0
        broadcast 192.168.4.255
EOL1
sudo rm -f /run/network/ifstate.ib0
sudo mv /home/cc/ib0 /etc/network/interfaces.d/ib0.cfg
sudo mv /home/cc/interfaces /etc/network/interfaces
sudo ifdown ib0
sudo ip addr flush dev ib0
sudo ifup ib0 
EOF
i=$((i+1))
done
