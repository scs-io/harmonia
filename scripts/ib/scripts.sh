#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
for mp in 1 2 3 4
	do
	for np in ${mp} $((mp*10)) #$(seq 2 ${np})
	    do
	    for ap in 0
	        do
	        for sp in 1024 
	            do
	            for dp in $1 
	                do
	                for op in 0 1
	                    do
				echo "mpirun -np ${np} ./harmonia -m ${mp} -d 0 -r 10 -a ${ap} -s ${sp} -i ${dp} -o ${op} -w 0 -b 32768 -x 0"
				sudo sh -c "echo 3 > /proc/sys/vm/drop_caches"
				mpirun -np ${np} ./harmonia -m ${mp} -d 0 -r 10 -a ${ap} -s ${sp} -i ${dp} -o ${op} -w 0 -b 32768 -x 0
				sudo sh -c "echo 3 > /proc/sys/vm/drop_caches"
			done
                    done
                done
            done
		done
	done



