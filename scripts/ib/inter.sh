#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
for mp in 1 2 3 4 5 6 7 8
do
	for np in ${mp} $((mp*2))  $((mp*4))
	do
		for device in "ram" "nvme" "ssd" "hdd"
		do
			for op in 0 1
                	do
				for wp in 1
				do
					for sp in $((62*1024))
					do
						source /home/cc/nfs/harmonia/scripts/dropcache.sh
						echo "mpirun -n $np /home/cc/nfs/harmonia/harmonia -m ${mp} -t 1984 -s ${sp} -i /media/${device} -r 5 -o $op -w $wp"
						mpirun -n $np /home/cc/nfs/harmonia/harmonia -m ${mp} -t 1984 -s ${sp} -i /media/${device} -r 5 -o $op -w $wp >> /home/cc/nfs/harmonia/logs/inter_64MB.log
						source /home/cc/nfs/harmonia/scripts/dropcache.sh
					done
				done
			done
		done
	done
done
