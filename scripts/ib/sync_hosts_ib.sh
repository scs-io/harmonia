#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

while true
do
  echo "Enter the string used to filter out your instances: "
  read -e grep_str
  res=`python ~/novahosts_ib.py $grep_str 2>&1`
  if [[ $res ]]
  then
    echo $res
    echo "Fail to run novahosts.py"
    exit -1
  fi
  nins=`cat /dev/shm/hosts | wc -l`
  if [[ $nins == 0 ]]
  then
    echo No instance found
    continue
  fi
  echo "These are the instances:"
  cat /dev/shm/hosts
  echo "Are they correct? [yes/no]:"
  read -e correct
  if [[ "$correct" == "yes" ]]
  then
    sudo sh -c "cat /dev/shm/hosts > /etc/hosts" && rm -f /dev/shm/hosts
    break
  elif [[ "$correct" == "no" ]]
  then
    continue
  fi
done
cat /etc/hosts | awk '{print $2}' | sort > ~/nodes

HOSTS=`cat /etc/hosts  | awk '{print $2}'`
IPS=`cat /etc/hosts  | awk '{print $1}'`

rm -f ~/.ssh/known_hosts
for host in $HOSTS
do
  ssh-keyscan -H $host >> ~/.ssh/known_hosts
done

for ip in $IPS
do
  ssh-keyscan -H $ip >> ~/.ssh/known_hosts
done
HOSTS=`cat /etc/hosts | grep -v ib | awk '{print $2}'`
IPS=`cat /etc/hosts | grep -v ib  | awk '{print $1}'`

for host in $HOSTS
do
  echo "Copying to node $host ..."
  scp -q /etc/hosts $host:/tmp/hosts
  scp -q ~/nodes $host:/home/cc/nodes
  scp -q ~/.ssh/* $host:/home/cc/.ssh/
  scp -q ~/.bashrc $host:/home/cc/.bashrc
  scp -q ~/.bash_aliases $host:/home/cc/.bash_aliases
done

echo "Synchronizing ..."
if mpssh > /dev/null 2>&1
then
  mpssh -bf ~/nodes "sudo mv /tmp/hosts /etc/hosts" > /dev/null 2>&1
else
  for host in ${HOSTS[@]}
  do
    ssh $host "sudo mv /tmp/hosts /etc/hosts"
    ssh $host "sudo cp -r /home/cc/.ssh/* /root/.ssh/"
  done
fi
