# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
clear
NODES=$(cat /home/cc/nfs/harmonia/scripts/nodes)
for server_node in $NODES
do    
scp /home/cc/software/MLNX_OFED_LINUX-4.1-1.0.2.0-ubuntu16.04-x86_64.tgz cc@${server_node}:/home/cc/software/MLNX_OFED_LINUX-4.1-1.0.2.0-ubuntu16.04-x86_64.tgz

echo "starting server $server_node"
ssh -t $server_node << EOF
cd /home/cc/software
sudo rm -rf MLNX_OFED_LINUX-4.1-1.0.2.0-ubuntu16.04-x86_64
tar -xvf MLNX_OFED_LINUX-4.1-1.0.2.0-ubuntu16.04-x86_64.tgz
cd MLNX_OFED_LINUX-4.1-1.0.2.0-ubuntu16.04-x86_64/
sudo ./mlnxofedinstall --add-kernel-support --all --force
sudo killall opensm
sudo rmmod iw_c2
sudo rmmod rdma_cm 
sudo rmmod ib_uverbs
sudo rmmod rdma_ucm
sudo rmmod ib_ucm
sudo rmmod ib_umad
sudo rmmod ib_ipoib
sudo rmmod mlx4_ib
sudo rmmod mlx4_en
sudo rmmod iw_cxgb3
sudo rmmod iw_cxgb4
sudo rmmod iw_nes
sudo /etc/init.d/openibd restart
EOF

done
