#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
if [ -d /etc/profile.d ]; then
  for i in /etc/profile.d/*.sh; do
    if [ -r $i ]; then
      . $i
    fi
  done
  unset i
fi
umount --force /mnt/1pfs 
umount --force /mnt/2pfs
umount --force /mnt/3pfs
umount --force /mnt/4pfs
ps -aef | grep pvfs | awk '{print $2}' | xargs kill -9
killall pvfs2-client
rmmod /home/cc/nfs/install/lib/modules/`uname -r`/kernel/fs/pvfs2/pvfs2.ko 
rm -r /mnt/*
mkdir /mnt/1pfs
mkdir /mnt/2pfs
mkdir /mnt/3pfs
mkdir /mnt/4pfs
chown cc:cc -R /mnt
echo "loading kernel module"
insmod /home/cc/nfs/install/lib/modules/`uname -r`/kernel/fs/pvfs2/pvfs2.ko
echo "loading pvfs2-client"
/home/cc/nfs/install/sbin/pvfs2-client -p /home/cc/nfs/install/sbin/pvfs2-client-core
echo "mounting the pvfs2"
mount -t pvfs2 tcp://hr2-bb-1:3334/orangefs /mnt/1pfs
mount -t pvfs2 tcp://hr2-bb-1:3335/orangefs /mnt/2pfs
mount -t pvfs2 tcp://hr2-bb-1:3336/orangefs /mnt/3pfs
mount -t pvfs2 tcp://hr2-bb-1:3337/orangefs /mnt/4pfs
mount | grep pvfs2
