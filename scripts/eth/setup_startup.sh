# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
clear
NODES=$(cat /home/cc/nfs/harmonia/scripts/eth/nodes)
i=20
for server_node in $NODES
do    
echo "starting server $server_node"
scp /home/cc/.bashrc $server_node:/home/cc/.bashrc
ssh -t $server_node << EOF
cat > /home/cc/startup.sh <<EOL
echo "run startup script"
export PATH=/home/cc/nfs/install/lib/:/home/cc/nfs/install/include/:/home/cc/nfs/install/bin:/home/cc/nfs/install/sbin:/home/cc/main/install/lib/:/home/cc/main/install/include/:/home/cc/main/install/bin:/home/cc/main/install/sbin:$PATH
export LD_LIBRARY_PATH=/home/cc/nfs/install/include:/home/cc/nfs/install/lib/:/home/cc/main/install/include:/home/cc/main/install/lib/:$LD_LIBRARY_PATH
export PVFS2_TAB=/etc/pvfs2tab
EOL
sudo mv /home/cc/startup.sh /etc/profile.d/startup.sh
sudo cp /home/cc/.bashrc /root/.bashrc
sudo ldconfig
EOF
i=$((i+1))
done
