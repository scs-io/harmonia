#!/bin/bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
SERVER_NODES=$(cat /home/cc/nfs/harmonia/scripts/eth/servers)

for server in 1 2 3 4
do
i=0
for server_node in $SERVER_NODES
do
if [ $i -lt $server ]
then

ssh $server_node "sudo rm -rf /mnt/ramdisk/${server}pfs/storage"
ssh $server_node "sudo mkdir -p /mnt/ramdisk/${server}pfs/storage"
ssh $server_node "sudo chown cc:cc /mnt/ramdisk/${server}pfs -R"
ssh $server_node "/home/cc/nfs/install/sbin/pvfs2-server -f -a ${server_node} /home/cc/nfs/harmonia/setup/conf/eth/${server}pfs_ram.conf"
echo "$server_node is starting"
ssh $server_node "/home/cc/nfs/install/sbin/pvfs2-server -a ${server_node} /home/cc/nfs/harmonia/setup/conf/eth/${server}pfs_ram.conf"
ssh $server_node "ps -aef | grep pvfs2"
echo "$server_node  has started"
ssh $server_node "ps -aef | grep pvfs2"
fi
i=$((i+1))
done
done
