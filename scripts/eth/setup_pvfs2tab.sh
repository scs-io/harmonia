# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
clear
NODES=$(cat /home/cc/nfs/harmonia/scripts/eth/nodes)
i=20
for server_node in $NODES
do    
echo "starting server $server_node"
ssh -t $server_node << EOF
cat > /home/cc/pvfs2tab <<EOL
tcp://hr2-bb-1:3334/orangefs /mnt/1pfs pvfs2 defaults 0 0
tcp://hr2-bb-1:3335/orangefs /mnt/2pfs pvfs2 defaults 0 0
tcp://hr2-bb-1:3336/orangefs /mnt/3pfs pvfs2 defaults 0 0
tcp://hr2-bb-1:3337/orangefs /mnt/4pfs pvfs2 defaults 0 0
EOL
sudo mv /home/cc/pvfs2tab /etc/pvfs2tab
EOF
i=$((i+1))
done
