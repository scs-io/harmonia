#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#sudo insmod /lib/modules/4.4.0-93-generic/kernel/fs/pvfs2/pvfs2.ko
sudo umount --force /media/ram/mount
sudo umount --force /media/nvme/mount
sudo umount --force /media/ssd/mount
sudo umount --force /media/hdd/mount
killall pvfs2-server
sudo killall pvfs2-client
declare -a devices=( [3]="hdd" )
declare -a ports=( [3]="3337")
sudo insmod /lib/modules/4.4.0-93-generic/kernel/fs/pvfs2/pvfs2.ko
for index in "${!devices[@]}";
do
	export device=${devices[${index}]}
	export port=${ports[$index]}
   	echo "Running $device"
	#read a;
	sudo chown cc:cc -R /media/$device
	cd /media/${device}
        mkdir -p mount orangefs/storage
 	rm -rf orangefs/storage/*
	pvfs2-server -f -a localhost ~/orangefsconf/${device}.conf
	pvfs2-server -a localhost ~/orangefsconf/${device}.conf
	echo "Server started for device $device"
	#read a;
	sudo /home/cc/software/install/sbin/pvfs2-client -p /home/cc/software/install/sbin/pvfs2-client-core
	echo "Client started $device $port"
	#read a;
	sudo mount -t pvfs2 tcp://localhost:${port}/orangefs /media/${device}/mount
	echo "Client mounted for device $device"
	ps -aef | grep pvfs2
	#read a;
	cd mount
	rm file*
	dd if=/dev/urandom of=../file_rank_0_group_0.txt bs=1048576 count=512
	pvfs2-cp -t ../file_rank_0_group_0.txt file_rank_0_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_1_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_2_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_3_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_4_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_5_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_6_group_0.txt
	pvfs2-cp -t file_rank_0_group_0.txt file_rank_7_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_8_group_0.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_9_group_0.txt

 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_0_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_1_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_2_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_3_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_4_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_5_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_6_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_7_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_8_group_1.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_9_group_1.txt

 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_0_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_1_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_2_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_3_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_4_group_2.txt
	pvfs2-cp -t file_rank_0_group_0.txt file_rank_5_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_6_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_7_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_8_group_2.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_9_group_2.txt

 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_0_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_1_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_2_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_3_group_3.txt
	pvfs2-cp -t file_rank_0_group_0.txt file_rank_4_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_5_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_6_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_7_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_8_group_3.txt
 	pvfs2-cp -t file_rank_0_group_0.txt file_rank_9_group_3.txt
	echo "copied ${device}"
  	#read a;
	cd ~/harmonia	
	rm run* test.log
	./scripts.sh /media/${device}/mount	
	mv test.log ${device}.log
	echo "script finished ${device}"	
	#read a;
	sudo umount /media/${device}/mount 
	sudo killall pvfs2-server
        sudo killall pvfs2-client
done

