#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
for mp in $2 
	do
	for np in $3 #$(($mp*32)) $(($mp*16)) $(($mp*4)) $(($mp*1)) #128  #$(seq 1 48)
	    do
	    for ap in 0 #1
	        do
	        for sp in $((1*1024)) #$((4*1024)) $((16*1024))
	            do
	            for dp in $1 
	                do
			for wp in 0 #1
			do
	                	for op in 0 #1
	                    	do
					echo "mpirun -np ${np} -f nodes$((${np}*4/${mp})) ./harmonia -m ${mp} -d 0 -r 5 -a ${ap} -s ${sp} -i ${dp} -o ${op} -w $wp -b $sp -x 0"
					sudo sh -c "echo 3 > /proc/sys/vm/drop_caches"
					mpirun -np ${np} -f /home/cc/nfs/harmonia/nodes$((${np}*4/${mp})) /home/cc/nfs/harmonia/harmonia -m ${mp} -d 0 -r 5 -a ${ap} -s ${sp} -i ${dp} -o ${op} -w $wp -b $sp -x 0 -l /home/cc/nfs/harmonia/logs
					sudo sh -c "echo 3 > /proc/sys/vm/drop_caches"
				done
			done
                    done
                done
            done
	done
done



