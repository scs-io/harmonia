#!/usr/bin/env bash
# Copyright (C) 2019  SCS Lab <scs-help@cs.iit.edu>, Hariharan Devarajan
# <hdevarajan@hawk.iit.edu>, Anthony Kougkas <akougkas@iit.edu>,
# Xian-He Sun <sun@iit.edu>
#
# This file is part of Harmonia
# 
# Harmonia is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#sudo insmod /lib/modules/4.4.0-93-generic/kernel/fs/pvfs2/pvfs2.ko
sudo umount --force /media/ram/mount
sudo umount --force /media/nvme/mount
sudo umount --force /media/ssd/mount
sudo umount --force /media/hdd/mount
killall pvfs2-server
sudo killall pvfs2-client
declare -a devices=([0]="ram" [1]="nvme" [2]="ssd" [3]="hdd" )
declare -a ports=( [0]="3334" [1]="3335" [2]="3336" [3]="3337")
sudo insmod /lib/modules/4.4.0-93-generic/kernel/fs/pvfs2/pvfs2.ko
for index in "${!devices[@]}";
do
	sudo umount --force /media/ram/mount
	sudo umount --force /media/nvme/mount
	sudo umount --force /media/ssd/mount
	sudo umount --force /media/hdd/mount
        sudo killall pvfs2-server
	sudo killall pvfs2-client
	export device=${devices[${index}]}
	export port=${ports[$index]}
   	echo "Running $device"
	#read a;
	sudo chown cc:cc -R /media/$device
	cd /media/${device}
        mkdir -p mount orangefs/storage
 	rm -rf orangefs/storage/*
	pvfs2-server -f -a localhost ~/orangefsconf/${device}.conf
	pvfs2-server -a localhost ~/orangefsconf/${device}.conf
	echo "Server started for device $device"
	#read a;
	sudo /home/cc/software/install/sbin/pvfs2-client -p /home/cc/software/install/sbin/pvfs2-client-core
	echo "Client started $device $port"
	#read a;
	sudo mount -t pvfs2 tcp://localhost:${port}/orangefs /media/${device}/mount
	echo "Client mounted for device $device"
	ps -aef | grep pvfs2
	#read a;
	cd mount
	rm file*
#	dd if=/dev/urandom of=../file_rank_0_group_0.txt bs=1048576 count=4096

	#pvfs2-cp -t ../file_rank_0_group_0.txt file_rank_0_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_1_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_2_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_3_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_4_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_5_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_6_group_0.txt
	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_7_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_8_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_9_group_0.txt

 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_10_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_11_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_12_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_13_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_14_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_15_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_16_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_17_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_18_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_19_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_20_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_21_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_22_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_23_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_24_group_0.txt
	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_25_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_26_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_27_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_28_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_29_group_0.txt

 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_30_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_31_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_32_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_33_group_0.txt
	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_34_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_35_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_36_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_37_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_38_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_39_group_0.txt
 
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_40_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_41_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_42_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_43_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_44_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_45_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_46_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_47_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_48_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_49_group_0.txt
	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_50_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_51_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_52_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_53_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_54_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_55_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_56_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_57_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_58_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_59_group_0.txt
 	#pvfs2-cp -t file_rank_0_group_0.txt file_rank_60_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_61_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_62_group_0.txt
        #pvfs2-cp -t file_rank_0_group_0.txt file_rank_63_group_0.txt
     	#echo "copied ${device}"
END  
	#read a;
	cd ~/harmonia	
	rm run* test.log
	./scripts2.sh /media/${device}/mount	
	mv test.log ${device}_stress.log
	echo "script finished ${device}"	
	#read a;
	sudo umount /media/${device}/mount 
	sudo killall pvfs2-server
        sudo killall pvfs2-client
done

